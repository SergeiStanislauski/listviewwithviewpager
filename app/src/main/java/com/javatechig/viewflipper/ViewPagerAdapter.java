package com.javatechig.viewflipper;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private ArrayList<ViewPagerItem> pagerItems;

    public ViewPagerAdapter(FragmentManager fm, ArrayList<ViewPagerItem> pagerItems) {
        super(fm);
        this.pagerItems = pagerItems;
    }

    @Override
    public Fragment getItem(int index) {
        return FrItemViewPager.newInstance(pagerItems.get(index));
    }

    @Override
    public int getCount() {
        return pagerItems.size();
    }
}