package com.javatechig.viewflipper;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ListViewAdapter extends BaseAdapter {
    private ArrayList<ListViewItem> mItems;
    private Context mContext;
    private FragmentManager mFragmentManager;

    public ListViewAdapter(FragmentActivity fragmentActivity, ArrayList<ListViewItem> items) {
        mContext = fragmentActivity;
        mItems = items;
        mFragmentManager = fragmentActivity.getSupportFragmentManager();
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View currentView, ViewGroup viewGroup) {
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //WARNING
        //All cells are stored in the RAM
        //may crash applications on low memory

        currentView = View.inflate(mContext, R.layout.item_listview, null);
        ViewPager viewPager = (ViewPager) currentView.findViewById(R.id.pager);
        viewPager.setId(position + 1);
        ViewPagerAdapter tempMyFriendPagerAdapter = new ViewPagerAdapter(mFragmentManager, mItems.get(position).getViewPagerItems());
        viewPager.setAdapter(tempMyFriendPagerAdapter);
        return currentView;
    }

    class Holder {
        TextView idRow;
        ViewPager viewPager;
    }
}


